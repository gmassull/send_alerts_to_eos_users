# python >= 3.5 only
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import json
import smtplib
import subprocess
import time

def send_email(json_item):
    warning = ".\n Please do not ignore this message. If you need a temporarily exception please open a Jira ticket ( https://its.cern.ch/jira ) selecting CMS VOC as project.\n The CMS VOC (Virtual Organization Contact)"
    msg = MIMEMultipart()
    msg['From'] = "cms-voc@cern.ch"
    msg['To'] = json_item['uid'] + "@cern.ch"
    msg['Subject'] = "EOS QUOTA OVER EXCEEDING THRESHOLD, ACTION REQUIRED"
    server_send = smtplib.SMTP('cernmx.cern.ch', 25)
    # this function receives uid as string, but here there is anyways a type check
    if isinstance(json_item["uid"], str):
        # json_item['space'] is like /eos/cms/store/user/
        # json_item['uid'] is the username
        if (json_item['statusbytes'] == "exceeded") and (json_item['statusfiles'] == "exceeded"):
            body = "Your quota " + json_item['space'] + json_item[
                'uid'] + " is exceeding maximum storage size and maximum number of files, please take action as soon as possible to go back beyond maximum allowed limits" + warning
        if (json_item['statusbytes'] == "exceeded") and (json_item['statusfiles'] != "exceeded"):
            body = "Your quota " + json_item['space'] + json_item[
                'uid'] + " is exceeding maximum storage size, please take action as soon as possible to go back beyond maximum allowed limits" + warning
        if (json_item['statusbytes'] != "exceeded") and (json_item['statusfiles'] == "exceeded"):
            body = "Your quota " + json_item['space'] + json_item[
                'uid'] + " is exceeding maximum number of files, please take action as soon as possible to go back beyond maximum allowed limits" + warning
        print(body)
        msg.attach(MIMEText(body, 'plain'))
        text = msg.as_string()
        server_send.sendmail(msg['From'], msg['To'], text)

def eos_query_and_parse():
    result = subprocess.run("export EOSHOME="" && eos -j root://eoscms.cern.ch quota ls -p /eos/cms/store/user/", shell=True, stdout=subprocess.PIPE)
    # if you want to read bash command output just run
    # print(result.stdout.decode('utf-8'))
    # or just run the bash command in a bash console
    json_data = json.loads(result.stdout.decode('utf-8'))
    for json_item in json_data['result']:
        # not all results contain an username, we need just results with an username
        if "uid" in json_item:
            # some usernames are numbers, they are inactive users. We cannot send emails to them
            # by using username@cern.ch because it does not work for them
            if isinstance(json_item["uid"], str):
                if (json_item['statusbytes'] == "exceeded") or (json_item['statusfiles'] == "exceeded"):
                    # You cannot send more than 50 unauthenticated emails per minute.
                    # Infos at https://readthedocs.web.cern.ch/display/MTA/%5BLP%5D+Sending+emails+with+Python
                    time.sleep(3)
                    send_email(json_item)


eos_query_and_parse()
